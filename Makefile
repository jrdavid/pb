executable = pb
sources = pb.py git.py
exe_dir = $(HOME)/bin
lib_dir = $(HOME)/lib/python
usr_dir = $(HOME)/usr/pb
install:
	install -m 755 -d $(exe_dir) $(lib_dir) $(usr_dir)
	install -D --mode=644 $(sources) $(lib_dir)
	install -D --mode=755 $(executable) $(exe_dir)
	cp --preserve=mode,timestamps -r ./templates $(usr_dir)
