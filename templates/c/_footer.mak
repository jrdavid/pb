$(_module)_objects := $(addsuffix .o, $(addprefix $($(_module)_output)/, $(notdir $(basename $(sources)))))
$(_module)_binary  := $($(_module)_output)/$(binary)

.PRECIOUS: $($(_module)_objects)

RESULT_POS  = "\033[40G"
TEXT_GREEN  = \033[32m
TEXT_RED    = \033[31m
TEXT_RESET  = \033[0m
RESULT_OK      = "[$(TEXT_GREEN)  OK  $(TEXT_RESET)]"
RESULT_FAILED  = "[$(TEXT_RED)FAILED$(TEXT_RESET)]"

$($(_module)_output)/%.o: ../$(_module)/src/%.c $($(_module)_output)/.f
	@printf "Compiling %s" $(notdir $<)
	@printf "%b" $(RESULT_POS)
	@printf "$(COMPILE.cc) -o $@ $<\n" >> $(build_log_file)
	@( $(COMPILE.cc) -o $@ $< >> $(build_log_file) 2>&1 \
		&& printf "%b\n" $(RESULT_OK) ) \
		|| ( printf "%b\n" $(RESULT_FAILED) && /bin/false )

$($(_module)_output)/%.o: ../$(_module)/src/%.cc  $($(_module)_output)/.f
	@printf "Compiling %s" $(notdir $<)
	@printf "%b" $(RESULT_POS)
	@printf "$(COMPILE.cc) -o $@ $<\n" >> $(build_log_file)
	@( $(COMPILE.cc) -o $@ $< >> $(build_log_file) 2>&1 \
		&& printf "%b\n" $(RESULT_OK) ) \
		|| ( printf "%b\n" $(RESULT_FAILED) && /bin/false )

$($(_module)_output)/%.a: $($(_module)_objects) $($(_module)_output)/.f
	@printf "Archiving in %s" $(notdir $@)
	@printf "%b" $(RESULT_POS)
	@printf "$(AR) r $@ $(filter %.o,$^)\n" >> $(build_log_file)
	@( $(AR) r $@ $(filter %.o,$^) >> $(build_log_file) 2>&1 \
		&& printf "%b\n" $(RESULT_OK) ) \
		|| ( printf "%b\n" $(RESULT_FAILED) && /bin/false )
	@printf "Indexing %s" $(notdir $@)
	@printf "%b" $(RESULT_POS)
	@( ranlib $@ >> $(build_log_file) 2>&1 \
		&& printf "%b\n" $(RESULT_OK) ) \
		|| ( printf "%b\n" $(RESULT_FAILED) && /bin/false )

$($(_module)_output)/%.so: $($(_module)_output)/.f
	@printf "Linking to shared library %s" $(notdir $@)
	@printf "%b" $(RESULT_POS)
	@printf "$(LINK.cc) -shared -Wsoname=$@ $(filter %.o,$^) -o $@\n" >> $(build_log_file)
	@( $(LINK.cc) -shared -Wl,soname,$@ $(filter %.a,$^) -o$@ >> $(build_log_file) 2>&1 \
		&& printf "%b\n" $(RESULT_OK) ) \
		|| ( printf "%b\n" $(RESULT_FAILED) && /bin/false )

$($(_module)_output)/%.elf:  $($(_module)_objects) $($(_module)_output)/.f
	@printf "Linking %s" $(notdir $@)
	@printf "%b" $(RESULT_POS)
	@printf "$(LINK.cc) $(filter %.a %.o,$^) -o $@\n" >> $(build_log_file)
	@( $(LINK.cc) $(filter %.a %.o,$^) -o$@ >> $(build_log_file) 2>&1 \
		&& printf "%b\n" $(RESULT_OK) ) \
		|| ( printf "%b\n" $(RESULT_FAILED) && /bin/false )

-include $($(_module)_objects:.o=.d)

.PHONY: all
all:: $($(_module)_binary)
$(_module): $($(_module)_binary)
