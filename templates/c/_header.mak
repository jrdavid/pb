_makefiles := $(filter-out _header.mak _footer.mak, $(MAKEFILE_LIST))
_module    := $(patsubst ../%,%,   \
						$(patsubst %/,%, \
							$(dir          \
								$(word $(words $(_makefiles)), $(_makefiles)))))
$(_module)_output := $(output_directory)/$(_module)
$($(_module)_output)/.f:
	@if ! [ -e $@ ] ; then \
		( mkdir -p -- $(patsubst %/.f,%,$@); \
		  touch $@ ) ; \
	fi
