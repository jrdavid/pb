include _header.mak

binary  =   acom.elf
sources = 	acom.cc \
				anoco.cc \
				anocorxframe.cc \
				averagebigdisplay.cc \
				averagetc6evs.cc \
				averageuniversal.cc \
				averageatmadvanced.cc \
				client.cc \
				clientrequestmsg.cc \
				controllerproxy.cc \
				ctrlaverage.cc \
				ctrleventlog.cc \
				ctrlhistory.cc \
				ctrlinterface.cc \
				ctrlinterfacemsg.cc \
				digit.cc \
				eventlogbigdisplay.cc \
				eventloguniversal.cc \
				eventlogatmadvanced.cc \
				historybigdisplay.cc \
				historytc6evs.cc \
				historyuniversal.cc \
				historyatmadvanced.cc \
				memorymgr.cc \
				parameter.cc \
				paramstring.cc \
				parameventlog.cc \
				requestmgr.cc \
				rmdevice.cc

ifeq "$(platform)" "acom2"
sources += \
				rxframe.cc \
				anocomodule.cc \
				anocomodulerxframe.cc\
				detector.cc \
				detectorrxframe.cc \
				thevco.cc \
				thevcorxframe.cc 
endif

ifneq "$(build_type)" "gdb"
	sources += bootloader.c
endif

ifeq "$(target_os)" "ecos"
sources +=  main.cc \
				acomhw.cc \
				gpio.cc
else
sources += main-linux.cc \
			  acomhw-stub.cc
endif

include _footer.mak
