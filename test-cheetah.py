from Cheetah.Template import Template

templateDef = """install:
	copy $binary to install/path/dir
"""

namespace = { 'binary' : 'foob' }
t = Template( file='foo.mak.tmpl', searchList=[namespace])
f = open('foo.mak', 'w')
f.write(str(t))
