#!/usr/bin/python
#
# Entry point for the pb program.
# The code in this file parses command-line options and calls on class Pb to do
# the actual work.
#
# TODO:
#   - list templates
#   - import templates into existing repo
#   - project rename
#   - completion

import argparse
import os
import pprint
import re
import shutil
import subprocess
import sys
import tarfile
import yaml

from Cheetah.Template import Template

import git

version = "1.0"

class PythonTemplates:
    def generate(self, work_path):
        print("Generating in {0}".format(work_path))

class Project:
    def __init__(self, name, context, language):
        self.name          = name
        self.file_name     = os.path.join( context.project_dir, "{0}.yaml".format(name))
        self.archive       = os.path.join( context.vault_dir,   "{0}.tar.gz".format(name))
        self.work_dir      = os.path.join( context.root, name )
        if language:
            self.templates_dir = os.path.join( context.templates_dir, language )
        self.public_dir    = os.path.join( context.public_dir, name )
        self.state         = "pristine"
        self.language      = language

    def save(self):
        stream = file(self.file_name, 'w')
        yaml.dump( self, stream, default_flow_style=False )

    def load(self):
        stream = file( self.file_name, 'r' )
        p = yaml.load(stream)
        try:
            self.name        = p.name
            self.state       = p.state
            self.language    = p.language
        except AttributeError:
            pass

    def import_project(self, options ):
        if not os.path.isdir(self.work_path):
            print("No directory named '{0}' exist in '{1}'.".format(
                    self.name, options.work))
            sys.exit(1)
        os.chdir(self.work_path)
        if not os.path.isfile( os.path.join( self.work_path, ".git" ) ):
            git.init(False)
        remotes = git.remote_list()
        if "public" not in remotes:
            git.remote_add("public", self.public_path)
        if not os.path.isdir( self.public_path ):
            os.makedirs( self.public_path )
            os.chdir( self.public_path )
            git.init(True)
        self.state = "open"

    def remove(self):
        # A bit dangerous...
        if os.path.isfile( self.file_name ):
            os.remove( self.file_name )
        if os.path.isfile( self.archive ):
            os.remove( self.archive )
        if os.path.isdir( self.work_dir ):
            shutil.rmtree( self.work_dir )
        if os.path.isdir( self.public_dir ):
            shutil.rmtree( self.public_dir )

    def populate_templates(self):
            if self.language == "python":
                print("Generating templates for python project.")
                templates = PythonTemplates()
            else:
                print("No templates for projects in '{0}'.".format(self.language))
                return
            templates.generate(self.work_path)

class Pb:
    def __init__(self, root):
        self.root          = root
        self.project_dir   = os.path.join(root, ".pb", "projects")
        self.vault_dir     = os.path.join(root, ".pb", "vault")
        self.templates_dir = os.path.join(root, ".pb", "templates")
        self.public_dir    = os.path.join(root, "public")
        self.languages = set()
        # Populate list of projects from 'projects' directory
        self.projects = dict()
        for filename in os.listdir(self.project_dir):
            name, ext = os.path.splitext(filename)
            project = Project( name, self, None )
            project.load()
            self.projects[name] = project
        # Populate list of known languages
        for filename in os.listdir(self.templates_dir):
            if os.path.isdir( os.path.join(self.templates_dir, filename) ):
                self.languages.add(filename)

    def list(self):
        """ List projects currently managed."""
        for p in self.projects.keys():
            print("{0} [{1}]".format(p, self.projects[p].state))
        print("{0} projects total.".format(len(self.projects.keys())))

    def create(self, project_name, language):
        """ Create a bare project. """
        if project_name in self.projects:
            print("A project named '{0}' already exists.".format(project_name))
            sys.exit(1)
        new_pb_project = Project(project_name, self, language)
        print("Creating project '{0}' ({1}).".format(project_name, language))
        new_pb_project.save()
        # Create bare project
        os.makedirs(new_pb_project.work_dir)
        original_dir = os.getcwd()
        os.chdir(new_pb_project.work_dir)
        if language in self.languages:
            print("I have templates for a '{0}' project.".format(language))
            for root, dirs, files in os.walk( new_pb_project.templates_dir ):
                # print root, dirs, files
                rel_dir = root[len(new_pb_project.templates_dir)+1:]
                for d in dirs:
                    o = os.path.join(root, d)
                    w = os.path.join(new_pb_project.work_dir, rel_dir, d)
                    os.makedirs(w)
                    print( "{0} -> {1}".format(o, w) )
                for f in files:
                    dest_file, ext = os.path.splitext(f)
                    o = os.path.join(root, f)
                    if ext == ".tmpl":
                        w = os.path.join(new_pb_project.work_dir, rel_dir, dest_file)
                        print( "{0} -> {1}".format(o, w) )
                        t = Template( file=o )
                        t.project = new_pb_project.name
                        of = open(dest_file, 'w')
                        of.write(str(t))
                    else:
                        w = os.path.join(new_pb_project.work_dir, rel_dir, f)
                        print( "{0} -> {1}".format(o, w) )
                        shutil.copy(o, w)
        else:
            print("No templates available.")

        print("Creating public git repo '{0}'".format(new_pb_project.public_dir))
        os.makedirs( new_pb_project.public_dir )
        original_dir = os.getcwd()
        os.chdir(new_pb_project.public_dir)
        git.init(True)
        os.chdir(original_dir)
        print("Initializing git repository.")
        git.init(False)
        print("Adding 'public' remote")
        git.remote_add("public", new_pb_project.public_dir)
        if language in self.languages:
            # Commit the templates
            git.add(["."])
            git.commit("Original import")
            # git.push("public")
        os.chdir(original_dir)

        # Open by default
        self.projects[project_name] = new_pb_project
        new_pb_project.state = "open"
        # why save? -> to update the state to 'open'
        new_pb_project.save()

    def remove(self, project_name):
        if not project_name in self.projects:
            print("No project named '{0}' is tracked.".format(project_name))
            sys.exit(1)
        p = self.projects[project_name]
        p.remove()

    def open(self, project_name):
        if not project_name in self.projects:
            print("No project named '{0}' is tracked.".format(project_name))
            sys.exit(1)
        project = Project(project_name, self, None)
        project.load()
        if project.state == "open":
            print("Project is already open.")
            sys.exit(1)
        old_cwd = os.getcwd()
        os.chdir( self.root )
        tarball = tarfile.open( project.archive, "r:gz" )
        tarball.extractall()
        os.remove( project.archive )
        project.state = "open"
        os.chdir( old_cwd )
        # why save? -> to update the state to 'open'
        project.save()

    def close(self, project_name):
        if not project_name in self.projects:
            print("No project named '{0}' is tracked.".format(project_name))
            sys.exit(1)
        project = Project(project_name, self, None)
        project.load()
        if project.state == "closed":
            print("Project is already closed.")
            sys.exit(1)
        old_cwd = os.getcwd()
        os.chdir( self.root )
        tarball = tarfile.open(project.archive, "w:gz")
        tarball.add( project.name )
        tarball.close()
        project.state = "closed"
        shutil.rmtree( project.work_dir )
        os.chdir( old_cwd )
        # why save? -> to update the state to 'closed'
        project.save()

    def import_project(self, directory):
        project_name = os.path.basename( directory )
        if project_name in self.projects:
            print("A project named '{0}' already exists.".format(project_name))
            sys.exit(1)
        # Create project entry in database
        new_pb_project = Project(project_name, self, None)
        # Copy files
        shutil.copytree( directory, new_pb_project.work_dir)
        original_dir = os.getcwd()
        print("Creating public git repo '{0}'".format(new_pb_project.public_dir))
        os.makedirs( new_pb_project.public_dir )
        original_dir = os.getcwd()
        os.chdir(new_pb_project.public_dir)
        git.init(True)
        os.chdir(new_pb_project.work_dir)
        # Initialize git repo if it doesn't exist
        if not os.path.isdir(".git"):
            # Create first import with initial content
            git.init(False)
            git.add(["."])
            git.commit("Import from {0}.".format(directory))
        # Create public repository
        if 'public' in git.remote_list():
            print("Renaming old public repo to public.orig")
            git.remote_rename('public', 'public.orig')
        print("Adding 'public' remote")
        git.remote_add("public", new_pb_project.public_dir)
        os.chdir( original_dir )
        # Open by default
        self.projects[project_name] = new_pb_project
        new_pb_project.state = "open"
        # why save? -> to update the state to 'open'
        new_pb_project.save()

    def check(self, directories):
        print("Directory\tVCS\tPublic repo")
        for directory in directories:
            original_dir = os.getcwd()
            if original_dir == self.root and directory == 'public':
                continue
            if not os.path.isdir( directory ):
                continue
            try:
                os.chdir(directory)
            except OSError:
                continue
            version_control = "n/a"
            public_repo     = "n/a"
            if os.path.isdir(".git"):
                version_control = "git"
                if 'public' in git.remote_list():
                    public_repo = "yes"
                else:
                    public_repo = "no"
            print("{0}\t{1}\t{2}".format(directory, version_control, public_repo))
            os.chdir(original_dir)

def init(directory):
    """ Initialize a new project repository """
    if not os.path.isdir(directory):
        os.makedirs(directory)
    os.chdir(directory)
    os.mkdir("public")
    os.mkdir(".pb")
    os.chdir(".pb")
    shutil.copytree( os.path.join(os.environ['HOME'], "usr/pb/templates"), "./templates")
    os.mkdir("projects")
    os.mkdir("vault")
    print("New repository created in {0}.".format(directory))

def get_root():
    while True:
        start_dir = os.getcwd()
        needle = os.path.join( start_dir, ".pb" )
        if os.path.isdir( needle ):
            return start_dir;
        if start_dir == '/':
            raise Exception("Not a lead repository.")
        os.chdir('..')



if __name__ == '__main__':

# Configure global options
    parser = argparse.ArgumentParser(
            prog="pb", description="Command-line project manager.")
    parser.add_argument('--version', action='version', version=''.join(['%(prog)s ', version]))
    parser.add_argument('-c', '--conf', help="Configuration directory.")

# Configure sub-commands
    subparsers = parser.add_subparsers(title="commands", dest='command') 

    parser_init  = subparsers.add_parser("init", help="Initialize a new pb repository.")
    parser_init.add_argument('directory', nargs="?", default=os.getcwd(), help="Root of new repository.")

    parser_create = subparsers.add_parser("create", help="Create a new project.")
    parser_create.add_argument('-l', '--language', help="Programming language for the project.")
    parser_create.add_argument('project', help="The name of the new project.")

    parser_import = subparsers.add_parser("import", help="Create a new project from an existing directory.")
    parser_import.add_argument('-l', '--language', default=None, help="Programming language for the project.")
    parser_import.add_argument('project', help="The name of the directory.")
    parser_import.add_argument('name', required=False, default=None, help="Name for the project (default is directory basename)")

    parser_remove = subparsers.add_parser("remove", 
            help="Remove a project from the tracked list.")
    parser_remove.add_argument('--clean', action="store_true", help="Also remove the project's work dir.")
    parser_remove.add_argument('project', help="The name of the project to remove.")

    parser_list = subparsers.add_parser("list", help="List tracked projects.")

    parser_open = subparsers.add_parser("open", help="Open an existing project.")
    parser_open.add_argument('project', help="The name of the project to open.")

    parser_close = subparsers.add_parser("close", help="Close an existing project.")
    parser_close.add_argument('project', help="The name of the project to close.")

    parser_check = subparsers.add_parser("check", help="Check state of given directory")
    parser_check.add_argument("directories", nargs='+', help="The directories to check")

# Do the actual parsing
    parse_result = parser.parse_args()

    if parse_result.command == 'init':
        init(parse_result.directory)
        sys.exit(0)

# Determine repository root
    try:
        root = get_root()
    except Exception as e:
        print e
        exit(1)

    pb = Pb(root)

    if   parse_result.command == 'list':
        pb.list()
    elif parse_result.command == 'create':
        pb.create(parse_result.project, parse_result.language)
    elif parse_result.command == 'remove':
        pb.remove(parse_result.project)
    elif parse_result.command == 'open':
        pb.open(parse_result.project)
    elif parse_result.command == 'close':
        pb.close(parse_result.project)
    elif parse_result.command == 'import':
        pb.import_project(parse_result.project)
    elif parse_result.command == 'check':
        pb.check(parse_result.directories)

# vim: ft=python tw=80
