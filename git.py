import subprocess
import os

def init(bare):
    command = ["git", "init" ]
    if bare:
        command.append("--bare")
    git = subprocess.Popen(command, stdout=subprocess.PIPE)
    output = git.communicate()
    err = git.wait()
    if err:
        raise RuntimeError, "'{0}' failed with code {1}".format(" ".join(command), err)

def remote_add(name, url):
    command = ["git", "remote", "add", name, url]
    git = subprocess.Popen(command)
    err = git.wait()
    if err:
        raise RuntimeError, "'{0}' failed with code {1}".format(" ".join(command), err)

def remote_list():
    command = ["git", "remote"]
    git = subprocess.Popen(command, stdout=subprocess.PIPE)
    output = git.communicate()[0]
    remotes = output.splitlines()
    err = git.wait()
    if err:
        raise RuntimeError, "'{0}' failed with code {1}".format(" ".join(command), err)
    return set(remotes)

def remote_rename(old, new):
    command = ["git", "remote", "rename", old, new]
    git = subprocess.Popen(command, stdout=subprocess.PIPE)
    output = git.communicate()[0]
    remotes = output.splitlines()
    err = git.wait()
    if err:
        raise RuntimeError, "'{0}' failed with code {1}".format(" ".join(command), err)
    return set(remotes)

def add(files):
    command = ["git", "add" ] + files
    git = subprocess.Popen(command)
    err = git.wait()
    if err:
        raise RuntimeError, "'{0}' failed with code {1}".format(" ".join(command), err)

def commit(message):
    command = ["git", "commit", "-m", message]
    git = subprocess.Popen(command)
    err = git.wait()
    if err:
        raise RuntimeError, "'{0}' failed with code {1}".format(" ".join(command), err)

def ignore(patterns):
    gitignore = open(".gitignore", "w")
    for p in patterns:
        gitignore.write(p + "\n")
    gitignore.close()

if __name__ == '__main__':
    remotes = remote_list()
    print remotes
