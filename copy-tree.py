import sys
import os
import shutil

source = 'foo'
dest = '/tmp'

def copy_tree(source, destination):
    abs_src = os.path.abspath(source)
    abs_dst = os.path.abspath(destination)
    src = os.path.split(abs_src)[1]
    dst = os.path.split(abs_dst)[1]
    print ("{0} -> {1}".format(abs_src, abs_dst))
    new_dir = os.path.join(abs_dst, src)
    print "mkdir {0}".format(new_dir)
    os.mkdir(new_dir)
    for e in os.listdir(abs_src):
        if os.path.isdir( os.path.join(abs_src, e) ):
            print "recurse {0}, {1}".format(os.path.join(abs_src, e), new_dir)
            copy_tree(os.path.join(abs_src, e), new_dir)
        else:
            print "copy {0} -> {1}".format(os.path.join(abs_src, e), os.path.join(new_dir, e))
            shutil.copyfile(os.path.join(abs_src, e), os.path.join(new_dir, e))

copy_tree(sys.argv[1], sys.argv[2])
